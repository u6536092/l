package com.example.u6536092.painting;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;



public class myView extends View{



    float xPos=0.0f;
    float yPos=0.0f;
    Bitmap painting;
    Canvas drawLine;
    public myView(Context context, @Nullable AttributeSet attrs) {  //constructor
        super(context, attrs);
    }
    protected void onSizeChanged(int w,int h,int oldw,int oldh){
        super.onSizeChanged(w,h,oldw,oldh);
        painting=Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
        drawLine=new Canvas(painting);

    }


    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint p=new Paint();
        p.setColor(Color.BLUE);
        canvas.drawBitmap(painting,0,0,p);
        drawLine.drawCircle(xPos,yPos,8,p);




    }
    public boolean onTouchEvent(MotionEvent me){
        xPos=me.getX();
        yPos=me.getY();
        invalidate();
        return true;
    }

}
