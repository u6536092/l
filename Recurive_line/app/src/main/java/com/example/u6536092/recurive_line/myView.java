package com.example.u6536092.recurive_line;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.Random;


public class myView extends View  implements Runnable{
    Handler time;
    Bitmap painting;
    Canvas drawLine;
    int step=0;
    public myView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        time=new Handler();
        time.postDelayed(this,100);
    }
    protected void onSizeChanged(int w,int h,int oldw,int oldh){
        super.onSizeChanged(w,h,oldw,oldh);
        painting = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawLine=new Canvas(painting);

    }
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint p=new Paint();
        p.setColor(Color.RED);
        p.setStrokeWidth(10);
        Random random=new Random();
        int x=random.nextInt(getWidth());
        int y=random.nextInt(getHeight());
        int xF=random.nextInt(getWidth());
        int yF=random.nextInt(getHeight());
         drawLine.drawLine(x,y,xF,yF,p);
        canvas.drawBitmap(painting,0,0,p);

    }

    @Override
    public void run() {
        if(step<51){
            step+=1;
            invalidate();

        }
        time.postDelayed(this,100);

    }
}
