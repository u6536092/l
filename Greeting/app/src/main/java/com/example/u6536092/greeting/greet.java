package com.example.u6536092.greeting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class greet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greet);
        Intent c=getIntent();
        String message=c.getStringExtra("p");
        TextView m=findViewById(R.id.n);
        m.setText(message);
    }
}
