package com.example.u6536092.counting;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class myView extends View implements Runnable{
    Handler timer;
    public myView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        timer=new Handler();
        timer.postDelayed(this,1000);
    }
    int counter=10;
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint p=new Paint();
        p.setColor(Color.RED);
        p.setStrokeWidth(10.0f);
        p.setTextSize(100.0f);

        canvas.drawText(counter+"",100,100,p);
    }
    public boolean onTouchEvent(MotionEvent me){

        counter=10;
        invalidate();
        return true;
    }



    @Override
    public void run() {
        if(counter<11 && counter>=0){
            counter--;
            invalidate();
        }

        timer.postDelayed(this,1000);
    }



}
